#include <stdio.h>

int main()
{
    int r1,c1,p,q,r2,c2,k,first[100][100],second[100][100],sum[100][100],multiply[100][100],mu=0;
    printf("Enter the rows and colomns of the first matrix\n");
    scanf("%d%d",&r1,&c1);
    printf("Enter the rows and colomns of the second matrix\n");
    scanf("%d%d",&r2,&c2);
    printf("Enter the first matrix elements :\n");

    for(p=0; p<r1; p++)
    {
        for(q=0; q<c1; q++)
        {
            scanf("%d",&first[p][q]);
        }
    }
    printf("Enter the second matrix elements:\n");

    for(p=0; p<r2; p++)
    {
        for(q=0; q<c2; q++)
        {
            scanf("%d",&second[p][q]);
        }
    }

    printf("The sum of matrix is :\n");

    for(p=0; p<r1; p++)
    {
        for(q=0; q<c1; q++)
        {
            sum[p][q]=first[p][q]+second[p][q];
            printf("%d\t",sum[p][q]);
        }
         printf("\n");
    }

   if(c1!=r2)
    {
        printf("Impossible!!!\n");
    }
    else
    {
        printf("The multiplication of matrix is:\n");

        for(p=0; p<r1; p++)
        {
            for(q=0; q<c2; q++)
            {

                for(k=0; k<r2; k++)
                {
                    mu=mu+first[p][k]*second[k][q];
                }
                multiply[p][q]=mu;
                mu=0;
            }

        }
    }

    for(p=0; p<r1; p++)
    {
        for(q=0; q<c2; q++)
        {
            printf("%d\t",multiply[p][q]);
        }
        printf("\n");
    }

    return 0;
}
