#include <stdio.h>

int main()
{
    char arr[1000],cha;
    int c=0;
    printf("Enter the string: ");
    gets(arr);
    printf("Enter the character that you want to know that frequency:");
    scanf("%c",&cha);

    for(int n=0; arr[n]!='\0'; ++n)
    {
        if(cha==arr[n])
            ++c;
    }

    printf("Frequency of %c character is %d",cha,c);
    return 0;
}
