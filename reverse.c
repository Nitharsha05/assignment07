#include <stdio.h>
#include <string.h>

int main()
{
    char c[100], re[100];
    int l, i, x, s, e;

    printf("Enter the sentences: ");
    gets(c);

    l   = strlen(c);
    x = 0;
    s = l - 1;
    e = l - 1;

    while(s > 0)
    {
        if(c[s] == ' ')
        {
            i = s + 1;
            while(i <= e)
            {
                re[x] = c[i];

                i++;
                x++;
            }
            re[x++] = ' ';

            e = s - 1;
        }

       s--;
    }

    for(i=0; i<=e; i++)
    {
        re[x] = c[i];
        x++;
    }

    re[x] = '\0';
    printf("Reverse form of sentences is \n%s", re);

    return 0;
}
